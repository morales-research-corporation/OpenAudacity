![OpenAudacity](https://gitlab.com/morales-research-corporation/OpenAudacity/blob/master/images/audacity.svg)
=============================

[![CMake Build](https://github.com/Morales-Research-Corporation/OpenAudacity/actions/workflows/cmake_build.yml/badge.svg)](https://github.com/Morales-Research-Corporation/OpenAudacity/actions/workflows/cmake_build.yml)
[![License](https://badgen.net/badge/license/GPLv3/blue)](LICENSE)
[![Open issues](https://badgen.net/github/open-issues/moralesresearch/OpenAudacity)](https://github.com/moralesresearch/OpenAudacity/issues)


**OpenAudacity** is an open-source, easy-to-use, multi-track audio editor and recorder for Windows, Mac OS X, GNU/Linux and other operating systems. Audacity is open source software licensed under GPL, version 2 or later.

- **Recording** from any real, or virtual audio device that is available to the host system.
- **Export / Import** a wide range of audio formats, extendible with FFmpeg.
- **High quality** using 32-bit float audio processing.
- **Plug-ins** Support for multiple audio plug-in formats, including VST, LV2, AU.
- **Macros** for chaining commands and batch processing.
- **Scripting** in Python, Perl, or any language that supports named pipes.
- **Nyquist** Very powerful built-in scripting language that may also be used to create plug-ins.
- **Editing** multi-track editing with sample accuracy and arbitrary sample rates.
- **Accessibility** for VI users.
- **Analysis and visualization** tools to analyze audio, or other signal data.

## Why did this project fork audacity/audacity?

You can find more information on the causes of the fork here:

- [**Privacy policy which may violate the original project's GPL license**](https://github.com/audacity/audacity/issues/1213)
- [**Contributor's License Agreement (CLA) which may violate the same GPL license**](https://github.com/audacity/audacity/discussions/932)
- [**Attempts at adding telemetry using Google services for data collection**](https://github.com/audacity/audacity/pull/835)

## Getting Started

For end users, the latest Windows and macOS release version of OpenAudacity is available in our GitHub release
and pre-fork binaries are located at [Audacity's Github release page](https://github.com/audacity/audacity/releases)

Build instructions are available [here](BUILDING.md).

More information for developers is available from the [Audacity Wiki](https://wiki.audacityteam.org/wiki/For_Developers).

Prebuilt binaries for the current release of OpenAudacity is located [here](https://github.com/moralesresearch/OpenAudacity/releases)

Submit issues at [Jira](https://moralesresearchcorp.atlassian.net/jira/software/c/projects/OP/issues/)
